/**
 * @file
 * Drupal CKEditor Tag plugin.
 */

/* global Drupal CKEDITOR */

(function (Drupal, CKEDITOR, $) {
  // The tag to handle.
  const tag = 'sitg';

  /**
   * Order by ASC.
   */
  function SortByName(x, y) {
    return ((x.attributes.ADRESSE == y.attributes.ADRESSE) ? 0 : ((x.attributes.ADRESSE > y.attributes.ADRESSE) ? 1 : -1));
  }

  /**
   * Select one address in the list.
   *
   * @param el -> element clicked in the list
   * @param address -> address input
   */
  function selectAddress(el, address) {

    // Remove results popup & Empty search input
    $('#idpadr_popup').hide()
    $('input[name ="idpadr_search"]').val('')

    // Update the description if empty.
    let descElement = $('input[name ="idpadr_param3"]')
    descElement.val(el.innerHTML)

    // Store IdpAdr.
    const idpAdr = el.getAttribute('id')
    let idpadrElement = $('input[name ="idpadr_param1"]')
    idpadrElement.val(idpAdr)
  }

  /**
   * Search Address from input value.
   *
   * @param addressElement
   */
  function searchAddress(addressElement) {
    let address = addressElement.val();
    let popup = $('#idpadr_popup');

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let response = JSON.parse(this.responseText);
        console.log(response, "response");
        let nbRes = response.features.length;

        // Check if addresses are return.
        if (nbRes > 0) {
          // Order address results.
          response.features.sort(SortByName);
          for (i in response.features) {
            let result = response.features[i].attributes;
            let el = document.createElement("p");
            el.setAttribute('id', result.IDPADR);
            el.innerHTML = result.ADRESSE + ' (' + result.NO_POSTAL + ' ' + result.NOM_NPA + ')';
            el.addEventListener("click", function () {
              selectAddress(this, addressElement);
            }, false);
            let input = document.createElement("input");
            input.setAttribute('type', 'hidden');
            input.setAttribute('value', '{IDPADR:}');
            popup.append(el);
          }
        }
        else {
          let el = document.createElement("p");
          el.innerHTML = 'Pas de resultat';
          popup.append(el);
        }
      }
    };

    // Search if there are more than 2 characters.
    if ((typeof address != 'undefined') && (address.length > 2)) {

      // Drop popup entries.
      popup.empty()
      popup.show()

      // Webservice call.
      address = address.replace(' ', '+')
      let str = address.toLowerCase()
      const IdpAdrEndpoint = drupalSettings.sitg.search
      const query = IdpAdrEndpoint.replace('%s', str)
      xhttp.open("GET", query, true)
      xhttp.send()
    }
  }

  $(window).on('dialogcreate', function (e, dialog, $element, settings) {

    // Trigger keyup on field search by address.
    $('input[name ="idpadr_search"]').bind('keyup', function(){
        searchAddress($('input[name ="idpadr_search"]'))
    });

    // Open the right tabulation.
    switch ($('input[name ="type"]').val()) {

      case 'appid':
        $('li.horizontal-tab-button-1 a').click();
        break;

      case 'webmap':
        $('li.horizontal-tab-button-2 a').click();
        break;

      case 'idpadr':
        $('li.horizontal-tab-button-3 a').click();
        break;

      default:
        // Latlong coordinates.
    }
  });

  /**
   * CKEditor plugin.
   */
  CKEDITOR.plugins.add(tag + 'editor', {
    // This plugin requires the Widgets System defined in the 'widget' plugin.
    requires: 'widget',
    icons: tag + 'editor',
    hidpi: true,

    // The plugin initialization logic goes inside this method.
    beforeInit: function (editor) {

      // Add custom CSS for CKEDITOR.
      editor.addContentsCss(this.path + 'css/' + tag + 'editor.css');

      // Configure CKEditor DTD for custom tag element.
      // @see https://www.drupal.org/node/2448449#comment-9717735
      let dtd = CKEDITOR.dtd, tagName;
      dtd[tag] = {'#': 1};
      // Register tag element as allowed child, in each tag that can contain a div element.
      for (tagName in dtd) {
        if (dtd[tagName].div) {
          dtd[tagName][tag] = 1;
        }
      }

      // Display the drupal-entity element inline.
      dtd.$inline[tag] = 1;

      // Generic command for adding/editing entities of all types.
      editor.addCommand('editor-' + tag, {
        allowedContent: tag + '[data-size,data-type,data-param1,data-param2,data-param3,data-param4]',
        requiredContent: tag + '[data-size,data-type,data-param1,data-param2,data-param3,data-param4]',
        inline: true,
        modes: { wysiwyg : 1 },
        canUndo: true,
        exec: function (editor, data) {
          const existingElement = Drupal.getSelectedTagSitg(editor, tag);

          let DOMElement = null;
          const existingValues = {};
          if (existingElement && existingElement.$ && existingElement.$.firstChild) {
            DOMElement = existingElement.$.firstChild;

            // Populate array with the current attributes value.
            existingValues['size'] = DOMElement.getAttribute('data-size');
            existingValues['type'] = DOMElement.getAttribute('data-type');
            existingValues['param1'] = DOMElement.getAttribute('data-param1');
            existingValues['param2'] = DOMElement.getAttribute('data-param2');
            existingValues['param3'] = DOMElement.getAttribute('data-param3');
            existingValues['param4'] = DOMElement.getAttribute('data-param4');
          }
          else {
            // On new Tag, use the selected text as default label.
            existingValues['label'] = editor.getSelection().getSelectedText();
          }

          // Dialog.
          const dialogSettings = {

            // Drupal.t() will not work inside CKEditor plugins because CKEditor
            // loads the JavaScript file instead of Drupal. Pull translated
            // strings from the plugin settings that are translated server-side.
            title: DOMElement ? editor.config[tag].EditorTag_dialogTitleEdit : editor.config[tag].EditorTag_dialogTitleAdd,
            dialogClass: tag + '-editor-dialog',
            resizable: true,
          };

          // Prepare a save callback to be used upon saving the dialog.
          const saveCallback = function (values) {
            const newTag = editor.document.createElement(tag);
            let tagLabel = 'SITG';
            if (values.latlong_param1 != '' && values.latlong_param2 != '') {

              // Coordinates (latlong).
              tagLabel += ' Latitude:' + values.latlong_param2 + ' Longitude:' + values.latlong_param1;
              newTag.setAttribute('data-size', values.latlong_size);
              newTag.setAttribute('data-type', 'latlong');
              newTag.setAttribute('data-param1', values.latlong_param1);
              newTag.setAttribute('data-param2', values.latlong_param2);
              newTag.setAttribute('data-param3', values.latlong_param3);
              newTag.setAttribute('data-param4', values.latlong_param4);
            }
            else if (values.appid_param1 != '') {

              // AppId.
              tagLabel += ' AppId:' + values.appid_param1;
              newTag.setAttribute('data-size', values.appid_size);
              newTag.setAttribute('data-type', 'appid');
              newTag.setAttribute('data-param1', values.appid_param1);
              newTag.setAttribute('data-param2', values.appid_param2);
              newTag.setAttribute('data-param3', '');
              newTag.setAttribute('data-param4', '');
            }
            else if (values.webmap_param1 != '') {

              // WebMap.
              tagLabel += ' WebMap:' + values.webmap_param1;
              newTag.setAttribute('data-size', values.webmap_size);
              newTag.setAttribute('data-type', 'webmap');
              newTag.setAttribute('data-param1', values.webmap_param1);
              newTag.setAttribute('data-param2', '');
              newTag.setAttribute('data-param3', '');
              newTag.setAttribute('data-param4', '');
            }
            else if (values.idpadr_param1 != '') {

              // Idpadr.
              tagLabel += ' IDPADR:' + values.idpadr_param1;
              newTag.setAttribute('data-size', values.idpadr_size);
              newTag.setAttribute('data-type', 'idpadr');
              newTag.setAttribute('data-param1', values.idpadr_param1);
              newTag.setAttribute('data-param2', values.idpadr_param2);
              newTag.setAttribute('data-param3', values.idpadr_param3);
              newTag.setAttribute('data-param4', '');
            }

            newTag.appendText(tagLabel);

            // If a previous element exists, assert it has been removed.
            if (existingElement && existingElement.$ && existingElement.$.firstChild) {
              existingElement.remove(false);
            }

            editor.insertHtml(newTag.getOuterHtml());
          };

          // Open the card dialog.
          Drupal.ckeditor.openDialog(editor, Drupal.url(`sitg/dialog/sitgeditor/${editor.config.drupal.format}`), existingValues, saveCallback, dialogSettings);
        }
      });

      // Register the tag widget.
      editor.widgets.add(tag, {

        // Minimum HTML which is required by this widget to work.
        allowedContent: tag + '[data-size,data-type,data-param1,data-param2,data-param3,data-param4]',
        requiredContent: tag + '[data-size,data-type,data-param1,data-param2,data-param3,data-param4]',

        // Simply recognize the element as our own. The inner markup if fetched
        // and inserted the init() callback, since it requires the actual DOM
        // element.
        upcast: function (element) {
          let attributes = element.attributes;
          if (attributes['data-size'] === undefined || attributes['data-type'] === undefined || attributes['data-param1'] === undefined || attributes['data-param2'] === undefined || attributes['data-param3'] === undefined || attributes['data-param4'] === undefined) {
            return;
          }
          return element;
        },

        // Downcast the element.
        downcast: function (element) {
          return element;
        }
      });

      // Add buttons for icon.
      if (editor.ui.addButton) {

        // Button name is case sensitive.
        // @see Drupal\sitg\Plugin\CKEditorPlugin\EditorTag::getButtons
        editor.ui.addButton('EditorTag' + tag, {
          label: Drupal.t('SITG'),
          command: 'editor-' + tag,
          icon: this.path + 'icons/' + tag + 'editor.png',
          allowedContent: tag + '[!data-size,!data-type,!data-param1,!data-param2,!data-param3,!data-param4]',
        });
      }

      // Execute widget editing action on double click.
      editor.on('doubleclick', function (evt) {
        const element = Drupal.getSelectedTagSitg(editor, tag);

        if (element !== null) {
          editor.execCommand('editor-' + tag);
        }
      });
    }
  });

  /**
   * Get the surrounding tag element of current selection.
   *
   * @example
   *  <doc>content</doc>
   *
   * @param {CKEDITOR.editor} editor
   *   The CKEditor editor object
   *
   * @return {?HTMLElement}
   *   The selected tag element, or null.
   */
  Drupal.getSelectedTagSitg = function (editor, tag) {
    const selection = editor.getSelection();
    let selectedElement = selection.getSelectedElement();
    if (!selectedElement) {
      selectedElement = selection.getStartElement();
    }

    if (selectedElement) {
      const widget = editor.widgets.getByElement(selectedElement, true);
      if (widget && widget.name === tag) {
        return selectedElement;
      }
    }

    return null;
  }

}(Drupal, CKEDITOR, jQuery));
