# SITG
Display a map in your content.

## Description
The Geneva Land Information System (SITG) provides you with a wide range of data through easy-to-access interactive maps and downloadable data.
More information on their website: https://ge.ch/sitg

## Requirements
This module requires SITG services.

## Installation
* Install the module  
(https://www.drupal.org/docs/extending-drupal/installing-modules).
* Add SITG to CKEditor  
(https://www.drupal.org/docs/user_guide/en/structure-text-format-config.html)
  1. In Admin >> Configuration >> Text formats and editors, choose your text format like "basic html".
  2. Add the SITG button in the toolbar
  3. Enable filter "Handle SITG Tag"
  4. In the allowed html tags, add:  
 `<sitg data-size data-type data-param1 data-param2 data-param3 data-param4>`
  5. Save
* Use it by adding an SITG map with the button in your wysiwyg toolbar, when editing a content.

## Configuration
You can edit the services endpoints here:  
Admin >> Configuration >> SITG >> Content authoring >> SITG

## Sponsor
* State of Geneva - https://www.ge.ch

## Maintainers
* Mickaël Silvert - https://www.drupal.org/user/yepa
