<?php

namespace Drupal\sitg\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Configure SITG settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * Constructs a EmbedSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StreamWrapperManagerInterface $stream_wrapper_manager) {
    parent::__construct($config_factory);
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sitg_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sitg.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sitg.settings');

    $form['display'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display settings'),
    ];

    $form['display']['large_map'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Large map'),
      '#default_value' => $config->get('large_map'),
    ];

    $form['display']['small_map'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Small map'),
      '#default_value' => $config->get('small_map'),
    ];

    $form['display']['very_small_map'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Very small map'),
      '#default_value' => $config->get('very_small_map'),
    ];

    $form['webmap'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Webmap'),
    ];

    $form['webmap']['webmap'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $config->get('webmap'),
      '#description' => $this->t('This string is used as a format by the PHP function sprintf.'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['webmap']['webmap_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS Class'),
      '#default_value' => $config->get('webmap_class'),
    ];

    $form['idpadr'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('IDPADR'),
    ];

    $form['idpadr']['idpadr'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $config->get('idpadr'),
      '#description' => $this->t('This string is used as a format by the PHP function sprintf.'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['idpadr']['idpadr_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS Class'),
      '#default_value' => $config->get('idpadr_class'),
    ];

    $form['coordinates'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Coordinates'),
    ];

    $form['coordinates']['coordinates'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $config->get('coordinates'),
      '#description' => $this->t('This string is used as a format by the PHP function sprintf.'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['coordinates']['coordinates_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS Class'),
      '#default_value' => $config->get('coordinates_class'),
    ];

    $form['appid'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('APPID'),
    ];

    $form['appid']['appid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $config->get('appid'),
      '#description' => $this->t('This string is used as a format by the PHP function sprintf.'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['appid']['appid_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS Class'),
      '#default_value' => $config->get('appid_class'),
    ];

    $form['conversion'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Conversion services'),
    ];

    $form['conversion']['ags1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AGS 1 Endpoint'),
      '#description' => $this->t('This string is used as a format by the PHP function sprintf.'),
      '#default_value' => $config->get('ags1'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['conversion']['ags2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AGS 2 Endpoint'),
      '#description' => $this->t('This string is used as a format by the PHP function sprintf.'),
      '#default_value' => $config->get('ags2'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['conversion']['idpAdr_to_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IdpAdr to Address Endpoint'),
      '#default_value' => $config->get('idpAdr_to_address'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['conversion']['idpAdr_from_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IdpAdr from Address Endpoint'),
      '#description' => $this->t('This string is used by the JS plugin (%s is the address parameter).'),
      '#default_value' => $config->get('idpAdr_from_address'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('sitg.settings');
    $config->set('large_map', $form_state->getValue('large_map'));
    $config->set('small_map', $form_state->getValue('small_map'));
    $config->set('very_small_map', $form_state->getValue('very_small_map'));
    $config->set('webmap', $form_state->getValue('webmap'));
    $config->set('webmap_class', $form_state->getValue('webmap_class'));
    $config->set('appid', $form_state->getValue('appid'));
    $config->set('appid_class', $form_state->getValue('appid_class'));
    $config->set('idpadr', $form_state->getValue('idpadr'));
    $config->set('idpadr_class', $form_state->getValue('idpadr_class'));
    $config->set('coordinates', $form_state->getValue('coordinates'));
    $config->set('coordinates_class', $form_state->getValue('coordinates_class'));
    $config->set('ags1', $form_state->getValue('ags1'));
    $config->set('ags2', $form_state->getValue('ags2'));
    $config->set('idpAdr_to_address', $form_state->getValue('idpAdr_to_address'));
    $config->set('idpAdr_from_address', $form_state->getValue('idpAdr_from_address'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
