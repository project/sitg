<?php

namespace Drupal\sitg\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;

/**
 * Provides a dialog for Editor Pop-in in editors.
 */
class SitgEditorDialog extends FormBase {

  /**
   * The Node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sitg_editor_dialog';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    $config = $this->configFactory->get('sitg.settings');

    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    $user_input = $form_state->getUserInput();
    $input = isset($user_input['editor_object']) ? $user_input['editor_object'] : [];

    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';

    $form['form-error-element'] = [
      '#type' => 'markup',
      '#markup' => '<div id="form-error-element"></div>',
    ];

    $form['startTabs'] = [
      '#type' => 'markup',
      '#markup' => '<div class=" field-group-tabs-wrapper"><div data-horizontal-tabs-panes="" class="horizontal-tabs-panes">',
    ];

    $form['by-coordinates'] = [
      '#type' => 'markup',
      '#markup' => '<details class="required-fields field-group-tab js-form-wrapper form-wrapper seven-details horizontal-tabs-pane" id="edit-group-by-coordinates" open="open">
      <summary role="button" aria-controls="edit-group-by-coordinates" aria-expanded="true" aria-pressed="true" class="seven-details__summary">' . $this->t('by coordinates') . '</summary>
      <div class="seven-details__wrapper details-wrapper">',
    ];

    $form['latlong_param2'] = [
      '#title' => $this->t('Latitude'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'latlong'  && isset($input['param2']) ? $input['param2'] : '',
      '#maxlength' => 120,
    ];

    $form['latlong_param1'] = [
      '#title' => $this->t('Longitude'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'latlong'  && isset($input['param1']) ? $input['param1'] : '',
      '#maxlength' => 120,
    ];

    $form['latlong_size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'select',
      '#default_value' => isset($input['size']) ? $input['size'] : '',
      '#options' => [
        'small' => $this->t('small'),
        'large' => $this->t('large'),
      ],
    ];

    $form['latlong_param3'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'latlong'  && isset($input['param3']) ? $input['param3'] : '',
      '#maxlength' => 120,
    ];

    $form['latlong_param4'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'latlong'  && isset($input['param4']) ? $input['param4'] : '',
      '#maxlength' => 120,
    ];

    $form['by-coordinates-end'] = [
      '#type' => 'markup',
      '#markup' => ' </div></details>',
    ];

    $form['by-app-id'] = [
      '#type' => 'markup',
      '#markup' => '<details class="required-fields field-group-tab js-form-wrapper form-wrapper seven-details horizontal-tabs-pane" id="edit-group-by-appId" open="open">
      <summary role="button" aria-controls="edit-group-by-appId" aria-expanded="true" aria-pressed="true" class="seven-details__summary">' . $this->t('by AppId') . '</summary>
      <div class="seven-details__wrapper details-wrapper">',
    ];

    $form['appid_param1'] = [
      '#title' => $this->t('AppId number'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'appid'  && isset($input['param1']) ? $input['param1'] : '',
      '#maxlength' => 120,
    ];

    $form['appid_size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'select',
      '#default_value' => isset($input['size']) ? $input['size'] : '',
      '#options' => [
        'small' => $this->t('small'),
        'large' => $this->t('large'),
      ],
    ];

    $form['appid_param2'] = [
      '#title' => $this->t('Settings'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'appid'  && isset($input['param2']) ? $input['param2'] : '',
      '#maxlength' => 120,
    ];

    $form['by-appId-end'] = [
      '#type' => 'markup',
      '#markup' => ' </div></details>',
    ];

    $form['by-web-map'] = [
      '#type' => 'markup',
      '#markup' => '<details class="required-fields field-group-tab js-form-wrapper form-wrapper seven-details horizontal-tabs-pane" id="edit-group-by-web-map" open="open">
      <summary role="button" aria-controls="edit-group-by--web-map" aria-expanded="true" aria-pressed="true" class="seven-details__summary">' . $this->t('by WebMap') . '</summary>
      <div class="seven-details__wrapper details-wrapper">',
    ];

    $form['webmap_param1'] = [
      '#title' => $this->t('WebMap number'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'webmap' && isset($input['param1']) ? $input['param1'] : '',
      '#maxlength' => 120,
    ];

    $form['webmap_size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'select',
      '#default_value' => isset($input['size']) ? $input['size'] : '',
      '#options' => [
        'small' => $this->t('small'),
        'large' => $this->t('large'),
      ],
    ];

    $form['by-web-map-end'] = [
      '#type' => 'markup',
      '#markup' => ' </div></details>',
    ];

    $form['by-address'] = [
      '#type' => 'markup',
      '#markup' => '<details class="required-fields field-group-tab js-form-wrapper form-wrapper seven-details horizontal-tabs-pane" id="edit-group-by-address" open="open">
      <summary role="button" aria-controls="edit-group-by-address" aria-expanded="true" aria-pressed="true" class="seven-details__summary">' . $this->t('by address') . '</summary>
      <div class="seven-details__wrapper details-wrapper">',
    ];

    $form['idpadr_search'] = [
      '#title' => $this->t('Search an address'),
      '#type' => 'textfield',
      '#default_value' => '',
      '#maxlength' => 120,
    ];

    $form['idpadr_popup'] = [
      '#type' => 'markup',
      '#markup' => '<div id="idpadr_popup"></div><div id="idpadr_res"></div>',
    ];

    $form['idpadr_param1'] = [
      '#title' => $this->t('IdpAdr'),
      '#type' => 'hidden',
      '#default_value' => $input['type'] == 'idpadr' && isset($input['param1']) ? $input['param1'] : '',
      '#maxlength' => 120,
    ];

    $form['idpadr_size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'select',
      '#default_value' => isset($input['size']) ? $input['size'] : '',
      '#options' => [
        'small' => $this->t('small'),
        'large' => $this->t('large'),
      ],
    ];

    $form['idpadr_param2'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'idpadr'  && isset($input['param2']) ? $input['param2'] : '',
      '#maxlength' => 120,
    ];

    $form['idpadr_param3'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textfield',
      '#default_value' => $input['type'] == 'idpadr'  && isset($input['param3']) ? $input['param3'] : '',
      '#maxlength' => 120,
    ];

    $form['by-address-end'] = [
      '#type' => 'markup',
      '#markup' => ' </div></details>',
    ];

    $form['type'] = [
      '#title' => $this->t('Type'),
      '#type' => 'hidden',
      '#default_value' => isset($input['type']) ? $input['type'] : '',
    ];

    $form['endTabs'] = [
      '#type' => 'markup',
      '#markup' => '</div></div>',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    $form['#attached']['drupalSettings']['sitg'] = ['search' => $config->get('idpAdr_from_address')];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $long = $form_state->getValue('latlong_param1');
    $lat = $form_state->getValue('latlong_param2');
    $appId = $form_state->getValue('appid_param1');
    $webMap = $form_state->getValue('webmap_param1');
    $idpAdr = $form_state->getValue('idpadr_param1');
    if (empty($idpAdr) && empty($webMap) && empty($appId)) {
      if (empty($lat)) {
        $form_state->setError($form['latlong_param2'], $this->t('Please enter a latitude.'));
      }
      if (empty($long)) {
        $form_state->setError($form['latlong_param1'], $this->t('Please enter a longitude.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      $formMessage['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#form-error-element', $formMessage));
    }
    else {
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

}
