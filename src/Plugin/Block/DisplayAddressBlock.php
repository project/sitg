<?php

namespace Drupal\sitg\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sitg\Service\ConversionService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get an SITG address from an idpAdr.
 *
 * @params
 * - idpadr: IDPADR address.
 * - entity_type: entity type like node or taxonomy_term.
 * - id: entity id.
 *
 * @Block(
 *   id = "sitg_address_idpadr",
 *   admin_label = @Translation("Display an SITG address from an idpAdr"),
 * )
 */
class DisplayAddressBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Conversion service.
   *
   * @var Drupal\sitg\Service\ConversionService
   */
  protected $conversionService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConversionService $conversion_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->conversionService = $conversion_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('sitg.conversion')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build($params = []) {
    $build['#theme'] = 'sitg_address_display';
    $idpAdr = 0;
    $entityType = '';
    $entityId = NULL;

    // Get idpAdr:
    if (isset($params['idpadr'])) {
      $idpAdr = $params['idpadr'];
    }

    if (!$idpAdr) {
      return $build;
    }

    // Get entity type:
    if (isset($params['entity_type'])) {
      $entityType = $params['entity_type'];
    }

    // Get entity id:
    if (isset($params['id'])) {
      $entityId = $params['id'];
    }

    $address = $this->conversionService->convertIdpAdrToAddress($idpAdr);

    if (!empty($address)) {
      $build['#address'] = $address;

      // Cache tag:
      if ($entityType && $entityId) {
        $build['#cache']['tags'][] = $entityType . ': ' . $entityId;
      }
    }

    return $build;
  }

}
