<?php

namespace Drupal\sitg\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sitg\Service\ConversionService;

/**
 * Display an SITG map from an idpAdr.
 *
 * @params
 * - idpadr: IDPADR address.
 * - entity_type: entity type like node or taxonomy_term.
 * - id: entity id.
 * - size (optional): define the iframe height through configurations settings.
 *      if not defined: small_map.
 *
 * @Block(
 *   id = "sitg_map_idpadr",
 *   admin_label = @Translation("Display an SITG map from an idpAdr"),
 * )
 */
class DisplayMapBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Conversion service.
   *
   * @var Drupal\sitg\Service\ConversionService
   */
  protected $conversionService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ConversionService $conversion_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->conversionService = $conversion_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('sitg.conversion')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build($params = []) {
    $build = [];
    $idpAdr = 0;
    $entityType = '';
    $entityId = NULL;

    // Get idpAdr:
    if (isset($params['idpadr'])) {
      $idpAdr = $params['idpadr'];
    }

    if (!$idpAdr) {
      return $build;
    }

    // Get entity type:
    if (isset($params['entity_type'])) {
      $entityType = $params['entity_type'];
    }

    // Get entity id:
    if (isset($params['id'])) {
      $entityId = $params['id'];
    }

    // Load SITG config.
    $sitgConfig = $this->configFactory->get('sitg.settings');

    // Get Url from idpAdr:
    $url = $this->conversionService->convertIdpAdrToUrl($idpAdr);

    if (!empty($url)) {
      $build['#theme'] = 'sitg_map_display';
      $build['#map']['class'] = $sitgConfig->get('webmap_class');
      $build['#map']['url'] = $url;

      // Get iframe height - if param exists.
      $build['#map']['height'] = $sitgConfig->get('small_map');
      if (isset($params['size'])) {
        $build['#map']['height'] = $sitgConfig->get($params['size']);
      }

      // Cache tag:
      if ($entityType && $entityId) {
        $build['#cache']['tags'][] = $entityType . ': ' . $entityId;
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();

    $form['idpadr'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IdpAdr'),
      '#default_value' => isset($config['idpadr']) ? $config['idpadr'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $idpAdr = $form_state->getValue('idpadr');

    if (!is_numeric($idpAdr)) {
      $form_state->setErrorByName('idpadr', $this->t('Needs to be an integer.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('idpadr', $form_state->getValue('idpadr'));
  }

}
