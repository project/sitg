<?php

namespace Drupal\sitg\Plugin\Filter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sitg\Service\ConversionService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\embed\DomHelperTrait;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Provides a filter to display Tag based on data attributes.
 *
 * @Filter(
 *   id = "sitg_editor_filter",
 *   title = @Translation("Handle SITG Tag"),
 *   description = @Translation("SITG data attributes: data-size,data-type,data-param1, data-param2, data-param3 & data-param4."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class SitgEditorFilter extends FilterBase implements ContainerFactoryPluginInterface {

  use DomHelperTrait;

  /**
   * The tag to handle.
   *
   * @var string
   */
  protected $tag = 'sitg';

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Conversion service.
   *
   * @var Drupal\sitg\Service\ConversionService
   */
  protected $conversionService;

  /**
   * Constructs a EditorTagFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\sitg\Service\ConversionService $conversion_service
   *   The SITG conversion service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $current_route_match, ConfigFactoryInterface $config_factory, ConversionService $conversion_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->currentRouteMatch = $current_route_match;
    $this->conversionService = $conversion_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('config.factory'),
      $container->get('sitg.conversion')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (strpos($text, '<' . $this->tag) !== FALSE) {
      $nid = 0;
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      // Get current nid.
      $node = $this->currentRouteMatch->getParameter('node');
      if ($node instanceof NodeInterface) {
        $nid = $node->id();
      }

      foreach ($xpath->query('//' . $this->tag . '[@data-param1]') as $tagElement) {
        $variables = [];
        $config = $this->configFactory->get('sitg.settings');
        $type = $tagElement->getAttribute('data-type');
        $size = $tagElement->getAttribute('data-size');

        // $type == 'latlong' => Longitude ||
        // $type == 'appid' => Id ||
        // $type == 'webmap' => Id ||
        // $type == 'idpadr' => Id
        $param1 = $tagElement->getAttribute('data-param1');

        // $type == 'latlong' => Latitude ||
        // $type == 'appid' => Parameters ||
        // $type == 'webmap' => Not Used ||
        // $type == 'idpadr' => Label
        $param2 = $tagElement->getAttribute('data-param2');

        // $type == 'latlong' => Label ||
        // $type == 'appid' => Not Used ||
        // $type == 'webmap' => Not Used ||
        // $type == 'idpadr' => Description
        $param3 = $tagElement->getAttribute('data-param3');

        // $type == 'latlong' => Description ||
        // $type == 'appid' => Not Used ||
        // $type == 'webmap' => Not Used ||
        // $type == 'idpadr' => Not Used
        $param4 = $tagElement->getAttribute('data-param4');

        $height = $size === 'small' ? $config->get('small_map') : $config->get('large_map');

        switch ($type) {

          case 'latlong':
            // Coordinates:
            $variables = [
              'label'   => $param3,
              'url'     => ($url = sprintf($config->get('coordinates'), $param1, $param2, $param4, $param3)) ? $url : '',
              'class'   => $config->get('coordinates_class'),
              'height'  => $height,
            ];
            break;

          case 'appid':
            $appIdUrl = ($url = sprintf($config->get('appid'), $param1)) ? $url : '';

            // Add settings:
            if (!empty($param2)) {
              $appIdUrl .= '&' . $param2;
            }

            $variables = [
              'label'   => $param3,
              'url'     => $appIdUrl,
              'class'   => $config->get('appid_class'),
              'height'  => $height,
            ];
            break;

          case 'webmap':
            $variables = [
              'label'   => $param3,
              'url'     => ($url = sprintf($config->get('webmap'), $param1)) ? $url : '',
              'class'   => $config->get('webmap_class'),
              'height'  => $height,
            ];
            break;

          case 'idpadr':
            $variables = [
              'label'   => $param3,
              'url'     => $this->conversionService->convertIdpAdrToUrl($param1, $param2, $param3),
              'class'   => $config->get('idpadr_class'),
              'height'  => $height,
            ];
            break;
        }

        $render = [
          '#theme' => 'sitg_editor',
          '#variables' => $variables,
          '#attached' => [
            'library' => [
              'sitg/sitg',
            ],
          ],
          '#cache' => [
            'tags' => [
              'tag:sitg',
              'node:' . $nid,
            ],
          ],
        ];

        // Bubble cache context to the page level.
        $result = $result->merge(BubbleableMetadata::createFromRenderArray($render));

        // Generate the output for rendering.
        $output = $this->renderer->render($render);

        $this->replaceNodeContent($tagElement, $output);
      }

      $result->setProcessedText(Html::serialize($dom));
    }

    return $result;
  }

}
