<?php

namespace Drupal\sitg\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;

/**
 * Defines a "SITG" plugin feature.
 *
 * @CKEditorPlugin(
 *   id = "sitgeditor",
 *   label = @Translation("SITG"),
 *   module = "sitg"
 * )
 */
class SitgEditor extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * The tag to handle.
   *
   * @var string
   */
  protected $tag = 'sitg';

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'sitg') . '/plugins/' . $this->getPluginId() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
//      'gech_tags/tags',@TODO
      'core/drupal.ajax',
      'field_group/core',
      'field_group/element.horizontal_tabs',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      $this->tag => [
        'EditorTag_dialogTitleAdd'  => $this->t('Insert SITG'),
        'EditorTag_dialogTitleEdit' => $this->t('Edit SITG'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'EditorTag' . $this->tag => [
        'label' => $this->t('Insert SITG'),
        'image' => drupal_get_path('module', 'sitg') . '/plugins/' . $this->getPluginId() . '/icons/' . $this->getPluginId() . '.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\editor\Form\EditorImageDialog
   * @see editor_image_upload_settings_form()
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    return $form;
  }

}
