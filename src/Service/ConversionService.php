<?php

namespace Drupal\sitg\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Conversion service for idpadr.
 */
class ConversionService {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Convert an IdpAdr to an URL from AGS services.
   *
   * @param string $idpadr
   *   The IDPADR.
   * @param string $label
   *   The Label.
   * @param string $description
   *   The description.
   *
   * @return string
   *   The Url.
   */
  public function convertIdpAdrToUrl($idpadr = '', $label = '', $description = '') {
    $lat = $long = '';

    // Load SITG config.
    $config = $this->configFactory->get('sitg.settings');

    // Transform gps point from vgs84 - transformation CH1903_To_WGS_1984_1.
    $client = \Drupal::httpClient();

    // Get from AGS1.
    $ags1Results = [];
    try {
      if ($url = sprintf($config->get('ags1'), $idpadr)) {
        $response = $client->get($url, [
          'verify' => FALSE,
          'verify_ssl' => FALSE,
          'http_errors' => FALSE,
          'headers' => [
            'Content-Type' => 'application/json',
          ],
        ]);

        if (in_array($response->getStatusCode(), [200, 304])) {
          $ags1Results = Json::decode($response->getBody()->getContents());
        }
      }
    }
    catch (RequestException $e) {
      watchdog_exception('sitg', $e);
    }

    if (isset($ags1Results['features'][0]['geometry'])) {
      $x = $ags1Results['features'][0]['geometry']['x'];
      $y = $ags1Results['features'][0]['geometry']['y'];

      // Get from AGS2.
      $ags2Results = [];
      try {
        if ($url = sprintf($config->get('ags2'), $x, $y)) {
          $response = $client->get($url, [
            'verify' => FALSE,
            'verify_ssl' => FALSE,
            'http_errors' => FALSE,
            'headers' => [
              'Content-Type' => 'application/json',
            ],
          ]);

          if (in_array($response->getStatusCode(), [200, 304])) {
            $ags2Results = Json::decode($response->getBody()->getContents());
          }
        }
      }
      catch (RequestException $e) {
        watchdog_exception('sitg', $e);
      }

      if (isset($ags2Results['geometries'][0])) {
        $lat = $ags2Results['geometries'][0]['x'];
        $long = $ags2Results['geometries'][0]['y'];
      }
    }

    if (!empty($lat) && !empty($long) && ($url = sprintf($config->get('idpadr'), $lat, $long, $description, $label))) {

      return $url;
    }
    else {

      return '';
    }
  }

  /**
   * Convert an IdpAdr to an address.
   *
   * @param string $idpadr
   *   The IDPADR.
   *
   * @return array
   *   The address in an array (street, zip and city indexes).
   */
  public function convertIdpAdrToAddress($idpadr = '') {

    // Load SITG config.
    $config = $this->configFactory->get('sitg.settings');

    // Transform gps point from vgs84 - transformation CH1903_To_WGS_1984_1.
    $client = \Drupal::httpClient();

    $query = [
      'returnGeometry' => FALSE,
      'outFields' => 'ADRESSE,IDPADR,NO_POSTAL,NOM_NPA',
      'f' => 'json',
      'where' => "IDPADR like '$idpadr'",
    ];
    $query_str = UrlHelper::buildQuery($query);
    $url = $config->get('idpAdr_to_address') . $query_str;

    $agsResults = [];
    try {
      $response = $client->get($url, [
        'verify' => FALSE,
        'verify_ssl' => FALSE,
        'http_errors' => FALSE,
        'headers' => [
          'Content-Type' => 'application/json',
        ],
      ]);

      $agsResults = Json::decode($response->getBody()->getContents());
    }
    catch (RequestException $e) {
      watchdog_exception('sitg', $e);
    }

    if (isset($agsResults['features'][0]['attributes'])) {
      $address['street'] = $agsResults['features'][0]['attributes']['ADRESSE'];
      $address['zip'] = $agsResults['features'][0]['attributes']['NO_POSTAL'];
      $address['city'] = $agsResults['features'][0]['attributes']['NOM_NPA'];

      return $address;
    }
    return [];

  }

}
